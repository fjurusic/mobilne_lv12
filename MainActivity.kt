package com.example.lv12

import TimePickerFragment
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        fun showTimePickerDialog(v: View) {
            TimePickerFragment().show(supportFragmentManager, "timePicker")
        }
    }
}